package Logica;

public class division implements operacion {
    @Override
    public double calcular(double a, double b) {
        if (b == 0) {
            throw new ArithmeticException();
        }
        return a / b;
    }
}