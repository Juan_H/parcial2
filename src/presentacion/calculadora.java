package presentacion;

import javax.swing.*;

import Logica.division;
import Logica.multiplicacion;
import Logica.operacion;
import Logica.resta;
import Logica.suma;
import calculadora.calculadora2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class calculadora extends JFrame {
    private JTextField txtDisplay;
    private double numero1, numero2;
    private String operador;
    private calculadora2 calculadora2;

    public calculadora() {
        this.calculadora2 = new calculadora2(); 
        inicializarComponentes();
    }

    private void inicializarComponentes() {
        setTitle("Calculadora");
        setSize(400, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        txtDisplay = new JTextField();
        txtDisplay.setEditable(false);
        txtDisplay.setHorizontalAlignment(JTextField.RIGHT);
        add(txtDisplay, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(5, 4));

        String[] botones = {
            "7", "8", "9", "/",
            "4", "5", "6", "*",
            "1", "2", "3", "-",
            "0", ".", "=", "+",
            "C"
        };

        for (String texto : botones) {
            JButton boton = new JButton(texto);
            boton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    botonPresionado(e.getActionCommand());
                }
            });
            panel.add(boton);
        }

        add(panel, BorderLayout.CENTER);
        setVisible(true);
    }

    private void botonPresionado(String texto) {
        if (texto.matches("[0-9.]")) {
            txtDisplay.setText(txtDisplay.getText() + texto);
        } else if (texto.matches("[/*\\-+]")) {
            operador = texto;
            numero1 = Double.parseDouble(txtDisplay.getText());
            txtDisplay.setText("");
        } else if (texto.equals("=")) {
            numero2 = Double.parseDouble(txtDisplay.getText());
            operacion operacion = null;

            switch (operador) {
                case "+":
                    operacion = new suma();
                    break;
                case "-":
                    operacion = new resta();
                    break;
                case "*":
                    operacion = new multiplicacion();
                    break;
                case "/":
                    operacion = new division();
                    break;
            }

            try {
                double resultado = new calculadora2().realizarOperacion(operacion, numero1, numero2);
                txtDisplay.setText(String.valueOf(resultado));
            } catch (ArithmeticException e) {
                txtDisplay.setText("Error");
            }
        } else if (texto.equals("C")) {
            txtDisplay.setText("");
            numero1 = 0;
            numero2 = 0;
            operador = "";
        }
    }


public static void main(String[] args) {
	new calculadora();
}
}



    